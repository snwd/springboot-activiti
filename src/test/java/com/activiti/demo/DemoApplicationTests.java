package com.activiti.demo;

import org.activiti.api.process.runtime.ProcessRuntime;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationTests {


    @Autowired
    private ProcessRuntime processRuntime;

    @Test
    void contextLoads() {
        System.out.println(processRuntime);
    }

}
